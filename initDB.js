var mongoose = require('mongoose');
var models   = require('./models');

// connect to database
var local_database_name = 'ineed';
var local_database_uri  = 'mongodb://localhost/' + local_database_name
var database_uri = process.env.MONGOHQ_URL || local_database_uri
mongoose.connect(database_uri);

var members = require('./data/members_seed.json');

models.Member
  .find()
  .remove()
  .exec(onceClear);

function onceClear(err) {
  if(err) console.log(err);

  var to_save_count = members.length;
  for(var i=0; i < members.length; i++) {
    var json = members[i];
    var member = new models.Member(json);

    member.save(function(err, member) {
      if(err) console.log(err);

      to_save_count--;
      console.log(to_save_count + ' left to save');

      if(to_save_count <= 0) {
        console.log('DONE');
        mongoose.connection.close()
      }
    });
  }
}
