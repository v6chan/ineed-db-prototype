var Mongoose = require('mongoose');

var memberSchema = new Mongoose.Schema({
  firstName:      { type: String, required: true },
  lastName:       { type: String, required: true },
  email:          { type: String, required: true },
  mobileNumber:   { type: String, required: true },
  preferences:    { type: [String], required: true }
});

var dealSchema = new Mongoose.Schema({
  dealName:     { type: String, required: true },
  vendorName:   { type: String, required: true },
  discount:     { type: Number, required: true },
  expire:       { type: Number, required: true }    // consider Date type after prototype
});

exports.Member = Mongoose.model('Member', memberSchema);
exports.Deal = Mongoose.model('Deal', dealSchema);

// Schema types for reference
// name:             String,
// restaurant:       String,
// googleAddress:    String,
// cost:             Number,
// waitTime:         Number,
// location:         String,
// description:      String,
// goals:            [String],
// restrictions:     [String],
// nutrition: {
//   ingredients:    String,
//   information:    Mongoose.Schema.Types.Mixed
// }