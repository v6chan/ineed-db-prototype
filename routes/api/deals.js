var express = require('express');
var router = express.Router();
var models = require('../../models');

// GET Requests

router.get('/', function(req, res) {
  models.Deal
    .find()
    .exec(response);

  function response(err, deals) {
    res.json(deals);
  }
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  models.Deal
    .findById(id)
    .exec(response);

  function response(err, deal) {
    if(deal) {
      res.json(deal);
    }
    else {
      res.status(404).end();
    }
  }
});

// POST Requests

router.post('/', function(req, res) {
  deal = new models.Deal({
    dealName:   req.body.dealName,
    vendorName: req.body.vendorName,
    discount:   req.body.discount,
    expire:     req.body.expire
  });

  deal.save(function(err, deal) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(deal);
    }
  });
});

// PUT Requests

router.put('/:id', function(req, res) {
  var id = req.param('id');

  models.Deal
    .findById(id)
    .exec(response);

  function response(err, deal) {
    if(deal) {
      deal.dealName     = (req.body.dealName || deal.dealName),
      deal.vendorName   = (req.body.vendorName || deal.vendorName),
      deal.discount     = (req.body.discount || deal.discount),
      deal.expire       = (req.body.expire || deal.expire)

      deal.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

// DELETE Requests

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  models.Deal
    .findById(id)
    .exec(response);

  function response(err, deal) {
    if(deal) {
      deal.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;