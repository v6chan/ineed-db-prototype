var express = require('express');
var router = express.Router();
var models = require('../../models');

router.get('/', function(req, res) {
  models.Member
    .find()
    .exec(response);

  function response(err, members) {
    res.json(members);
  }
});

router.post('/', function(req, res) {
  member = new models.Member({
    firstName:    req.body.firstName,
    lastName:     req.body.lastName,
    email:        req.body.email,
    mobileNumber: req.body.mobileNumber,
    preferences:  req.body.preferences
  });

  member.save(function(err, member) {
    if(err) {
      res.status(400).end();
    }
    else {
      res.json(member);
    }
  });
});

router.get('/:id', function(req, res) {
  var id = req.param('id');

  models.Member
    .findById(id)
    .exec(response);

  function response(err, member) {
    if(member) {
      res.json(member);
    }
    else {
      res.status(404).end();
    }
  }
});

router.put('/:id', function(req, res) {
  var id = req.param('id');

  models.Member
    .findById(id)
    .exec(response);

  function response(err, member) {
    if(member) {
      member.firstName    = (req.body.firstName || member.firstName),
      member.lastName     = (req.body.lastName || member.lastName),
      member.email        = (req.body.email || member.email),
      member.mobileNumber = (req.body.mobileNumber || member.mobileNumber),
      member.preferences  = (req.body.preferences || member.preferences)

      member.save(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

router.delete('/:id', function(req, res) {
  var id = req.param('id');

  models.Member
    .findById(id)
    .exec(response);

  function response(err, member) {
    if(member) {
      member.remove(function (err) {
        if(err) {
          res.status(500).end();
        } 
        else {
          res.status(200).end();
        }
      });
    }
    else {
      res.status(404).end();
    }
  }
});

module.exports = router;